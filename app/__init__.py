from flask import Flask, redirect, url_for, request, jsonify
from .dao import DAO
from bson.json_util import dumps


"""
    Move this into another file eventually
"""
app = Flask(__name__)
dao = DAO()


# ROUTES
@app.route('/')
def index():
    return 'Hello World!'


@app.route('/goats', methods=['POST', 'GET'])
def goats():
    if request.method == 'GET':
        # Get all documents
        goats = dao.get_goats()
        return jsonify({'goats': goats})
    else:
        pass


@app.route('/dogs', methods=['POST', 'GET'])
def dogs():
    if request.method == 'GET':
        # Get all documents
        dogs = dao.get_dogs()
        return jsonify(dumps(dogs))
    else:
        pass
