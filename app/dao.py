from .database import dogs, goats
from mysql.connector import connection


class DAO:

    def __init__(self):
        db = MongoClient(
            'mongodb+srv://lachenbockfarm:x25jky@cluster0-tzuux.mongodb.net/test?retryWrites=true&w=majority'
        ).get_database()
        self.dogs = dogs.Dogs(db)
        self.goats = goats.Goats(db)

    def get_goats(self):
        return self.goats.find_all()

    def get_dogs(self):
        return self.dogs.find_all()


