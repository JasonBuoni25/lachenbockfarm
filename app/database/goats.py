import pymongo


class Goats(object):

    def __init__(self, database):
        self.collection = database.goats

    def find_all(self):
        return self.collection.find().sort('id', pymongo.ASCENDING)