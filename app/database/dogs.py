import pymongo


class Dogs(object):

    def __init__(self, database):
        self.collection = database.dogs

    def find_all(self):
        return self.collection.find()
