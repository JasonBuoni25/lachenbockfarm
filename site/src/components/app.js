import React from 'react';
import { HashRouter, Switch, Redirect, Route } from 'react-router-dom';
import HomePage from './Home';
import TheFarm from './About/TheFarm';
import TheGoats from './About/TheGoats';
import TheDogs from './About/TheDogs';
import OurDogs from './dogs/OurDogs';
import PuppyInfo from './dogs/PuppyInfo';
import PuppiesPage from './dogs/Puppies';
import SalesPolicyPage from './sales-policy';
import MiniNubian from './goats/MiniNubian';
import BreedingSchedule from './goats/BreedingSchedule';
import NubianDoes from './goats/NubianDoes';
import Bucks from './goats/Bucks';
import ReferenceGoats from './goats/ReferenceGoats';
import Kids from './goats/Kids';
import NavigationBar from './Navigation'
import Footer from './Footer'

export default function App() {
  return (
    <HashRouter>
      <div className="site-header">
        <NavigationBar />
      </div>
      <div className='site-content'>
        <Switch>
            <Route exact path="/" render={() => <Redirect to="/home" />} />
            <Route path="/home" component={HomePage} />
            <Route path="/about" component={TheFarm} />
            <Route path="/dogs" component={TheDogs} />
            <Route path="/ourgoats" component={TheGoats} />
            <Route path="/ourdogs" component={OurDogs} />
            <Route path="/puppies" component={PuppiesPage} />
            <Route path="/puppyinfo" component={PuppyInfo} />
            <Route path="/kids" component={Kids} />
            <Route path="/mininubian" component={MiniNubian} />
            <Route path="/breeding" component={BreedingSchedule} />
            <Route path="/reference" component={ReferenceGoats} />
            <Route path="/bucks" component={Bucks} />
            <Route path="/standarddoes" component={NubianDoes} />
            <Route path="/sales" component={SalesPolicyPage} />
            <Route render={() => <Redirect to="/home" />} />
        </Switch>
      </div>
      <Footer />
    </HashRouter>
  );
}
