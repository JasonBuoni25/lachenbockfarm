import React, { Component } from 'react'

export default class Dropdown extends Component{
  constructor(props){
    super(props);
    this.close = this.close.bind(this)
  }

  state = {
    listOpen: false,
    headerTitle: this.props.title,
    list: this.props.list
  };

  componentDidUpdate(){
    const { listOpen } = this.state;
    setTimeout(() => {
      if(listOpen){
        window.addEventListener('click', this.close)
      }
      else{
        window.removeEventListener('click', this.close)
      }
    }, 0)
  }

  componentWillUnmount(){
    window.removeEventListener('click', this.close)
  }

  close(timeOut){
    this.setState({
      listOpen: false
    })
  }

  selectItem(title, id, stateKey){
    const list = this.state.list.map((item) => {
      if(item.id === id) {
        item.selected = true;
      } else {
        item.selected = false;
      }

      return item;
    });

    this.setState({ listOpen: false }, () => {
      this.props.onSelect(id, this.props.stateItem);
    });

  }

  toggleList(){
    this.setState(prevState => ({
      listOpen: !prevState.listOpen
    }))
  }

  render(){
    const{listOpen, headerTitle, list} = this.state;
    return(
      <div className="dd-wrapper">
        <div className="dd-header" onClick={() => this.toggleList()}>
          <div className="dd-header-title">{headerTitle}</div>
        </div>
        {listOpen && <ul className="dd-list" onClick={e => e.stopPropagation()}>
          {list.map((item)=> (
            <li className={`dd-list-item ${item.selected && 'selected-nav-item' || null}`} key={item.id} onClick={() => this.selectItem(item.title, item.id, item.key)}>{item.title}</li>
          ))}
        </ul>}
      </div>
    )
  }
}
