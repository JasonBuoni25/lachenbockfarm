import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { Link } from 'react-router-dom';
import Dropdown from './Dropdown';
import '../../styles/navigation.less';

export default class NavigationBar extends Component {

    constructor(props) {
        super(props);
        this.onSelectedChanged = this.onSelectedChanged.bind(this);
        this.onTabChange = this.onTabChange.bind(this);
    }

    state = {
        redirect: false,
        currentTab: '',
        aboutDropdown: [
            {
                id: 0,
                title: 'The Farm',
                selected: false,
                key: 'about'
            },
            {
                id: 1,
                title: 'The Goats',
                selected: false,
                key: 'ourgoats'
            },
            {
                id: 2,
                title: 'The Dogs',
                selected: false,
                key: 'dogs'
            },
        ],
        goatsDropdown: [
            {
                id: 0,
                title: 'Mini Nubian Does',
                selected: false,
                key: 'mininubian'
            },
            {
                id: 1,
                title: 'Standard Nubian Does',
                selected: false,
                key: 'standarddoes'
            },
            {
                id: 2,
                title: 'Bucks',
                selected: false,
                key: 'bucks'
            },
            {
                id: 3,
                title: 'Breeding Schedule',
                selected: false,
                key: 'breeding'
            },
            {
                id: 4,
                title: 'For Sale',
                selected: false,
                key: 'kids'
            },
            {
                id: 5,
                title: 'Reference Goats',
                selected: false,
                key: 'reference'
            },
        ],
        dogsDropdown: [
            {
                id: 0,
                title: 'Our Dogs',
                selected: false,
                key: 'ourDogs'
            },
            {
                id: 1,
                title: 'Puppies for Sale',
                selected: false,
                key: 'puppies'
            },
            {
                id: 2,
                title: 'Information',
                selected: false,
                key: 'puppyinfo'
            }
        ]
    };

    componentDidMount() {
        const url = window.location.href;
        const currentTab = url.split('/').pop();
        this.setState({ currentTab });
    }

    onSelectedChanged(id, stateItem) {
        const page = this.state[stateItem].find((item) => item.id === id);
        this.setState({
            currentTab: stateItem,
            redirect: true,
            page: `/${page.key}`
        });
    }

    onTabChange(currentTab) {
        this.setState({ currentTab, redirect: false });
    }

    render() {
        const { currentTab, redirect, page } = this.state;

        return (
            <>
                <div className="nav-bar">
                    <div className="nav nav-brand" >
                        <Link onClick={() => this.onTabChange('home')} to="/Home" ><i className="fas fa-tractor padding-right-5"></i>Lachenbock Farm</Link>
                    </div>
                    <div className="nav-right">
                        <span className={`nav nav-link ${currentTab === 'aboutDropdown' && 'nav-selected' || ''}`}>
                            <Dropdown stateItem="aboutDropdown" title='About' list={this.state.aboutDropdown} onSelect={this.onSelectedChanged}/>
                        </span>
                        <span className={`nav nav-link ${currentTab === 'goatsDropdown' && 'nav-selected' || ''}`}>
                            <Dropdown stateItem="goatsDropdown" title='Goats' list={this.state.goatsDropdown} onSelect={this.onSelectedChanged}/>
                        </span>
                        <span className={`nav nav-link ${currentTab === 'dogsDropdown' && 'nav-selected' || ''}`}>
                            <Dropdown stateItem="dogsDropdown" title='Great Pyreneese' list={this.state.dogsDropdown} onSelect={this.onSelectedChanged} />
                        </span>
                        <span className={`nav nav-link ${currentTab === 'sales' && 'nav-selected' || ''}`}>
                            <Link onClick={() => this.onTabChange('sales')} to="/sales" >Our Sales Policy</Link>
                        </span>
                    </div>
                </div>

                { redirect && <Redirect push to={page} /> || null}
            </>
        );
    }
}
