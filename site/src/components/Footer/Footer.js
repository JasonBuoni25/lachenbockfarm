import React, { Component } from 'react';
// import { SocialIcon } from 'react-social-icons';
import EmailModal from './EmailModal';
import '../../styles/footer.less';

export default class Footer extends Component {
    constructor(props) {
        super(props);
        this.onEmailButtonClick = this.onEmailButtonClick.bind(this);
        this.onFacebookClick = this.onFacebookClick.bind(this);
    }

    state = { showModal: false };

    onEmailButtonClick(e) {
        this.setState({ showModal: !this.state.showModal });
    }

    onFacebookClick(e) {
        const win = window.open('https://www.facebook.com/lachenbockfarm/', '_blank');
        win.focus();
    }

    render() {
        return (
            <>
                <div className="footer">
                    <div className="footer-left">© Lachenbock Farm 2019</div>
                    <div className="footer-right">
                        <span onClick={this.onEmailButtonClick}><i className="far fa-envelope-open fa-2x icon"></i></span>
                        <span onClick={this.onFacebookClick}><i className="fab fa-facebook fa-2x icon"></i></span>
                    </div>
                </div>
                <EmailModal show={this.state.showModal} onHide={this.onEmailButtonClick} />
            </>
        );
    }
}