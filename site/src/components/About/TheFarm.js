import React from 'react';
import Fade from 'react-reveal/Fade';
import TopHeader from '../TopHeader';
import homepagetop from '../../images/homepagetop.jpg';
import '../../styles/about.less';

export default function TheFarm(props) {
    return (
        <div className="home-content">
            <TopHeader text="The Farm" lineWidth="home-line" />
            <Fade left>
                <div className="home-header">
                    <img src={homepagetop} alt="farm" className="home-page-photo"/>
                </div>
            </Fade>
            <div className="site-text-content">
                <Fade right>
                    <div className="welcome-text">
                        <p>
                            Lachenbock Farm (pronounced <i>Lauk-en-bok</i>), which means "laughing buck" in German is a family owned and operated 230 acre homestead located in Walker County in the beautiful mountains of Northwest Georgia.
                            We raise Standard Nubian goats, Miniature Nubian goats and Great Pyrenees livestock guardian dogs. We also have several horses and miniature donkeys.
                        </p>
                        <p>
                            All goats are registered with the MDGA or ADGA. Our Great Pyrenees are AKC registered with hip certification through OFA. We take pride in raising and breeding quality animals that are healthy, sound, have good temperaments and meet breed standards.
                            Currently we do not offer products for sale such as milk, cheese or soap....but we're working towards that goal. A milk/cheese parlor that meets FDA approval is coming in the near future as well as a licence to sell raw goats milk.
                        </p>
                    </div>
                </Fade>
                <Fade bottom>
                    <div className="about-text">
                        <p>
                            Although we are not an organic farm and will never seek organic status, we strive to manage our farm balancing both natural and conventional methods. We grow and bale our own non GMO orchard grass/alfalfa hay mix, fertilized as needed with chicken manure and refrain from using pesticides/herbicides on our fields. We believe you are what you eat...and that includes livestock.
                            Our animal husbandry protocols do include conventional medications on an as needed basis. We also vaccinate our livestock, considering it cheap insurance.
                        </p>
                        <p>
                            Our deworming protocols include natural methods such as COWP, Sericea Lespedeza, pasture rotation and feeding herbs with anti-parasitic properties, using chemical wormers only when necessary.
                            Feel free to browse our site and learn about our farm and livestock. Email us with any questions or comments. We do our best to respond within 24-48 hours. We value your input and look forward to hearing from you.
                        </p>
                    </div>
                </Fade>
            </div>
            <div className="home-footer">

            </div>
        </div>
    );
}
