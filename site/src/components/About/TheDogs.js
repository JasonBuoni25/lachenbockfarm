import React from 'react';
import Fade from 'react-reveal/Fade';
import TopHeader from '../TopHeader';
import ourdogs from '../../images/ourdogs.jpg';
import '../../styles/about.less';

export default function TheFarm(props) {
    return (
        <div className="home-content">
            <TopHeader text="The Dogs" lineWidth="about-line" />
            <Fade left>
                <div className="home-header">
                    <img src={ourdogs} alt="farm" className="home-page-photo our-dogs-img"/>
                </div>
            </Fade>
            <div className="site-text-content padding-15-top">
                <Fade right>
                    <div className="welcome-text">
                        <p>
                            Great Pyrenees, also known as the Pyrenean Mountain Dog, have been used for centuries by shepherds to protect livestock. AKC standards describe them as "strong willed, independent and somewhat reserved, yet attentive, fearless and loyal to his charges both human and animal."
                        </p>
                        <p>
                            Pyrenees are massive dogs, regal in appearance. They have keen eyesight, making them tall enough to see trouble coming from a distance and have the size to intimidate and take on most predators. They will fearlessly protect their stock, at their own peril if necessary. A thick double coat enables these dogs to actually prefer cold weather and live among stock in the pasture year round.
                        </p>
                        <p>
                            Pyrenees also make wonderful pets. They're affectionate and loyal but require space to run and daily exercise. There are couple "musts" to having a Pyrenees as a pet. First must is a fenced in yard. Pyrenees see themselves as the caretaker of their kingdom. Downside to that is, to a Pyrenees their kingdom is a far as the eye can see. Without good fencing all Pyrenees will roam. No amount of training or exercise will keep them from roaming.
                        </p>
                        <p>
                            Second must is a good vacuum cleaner. These dogs were bred to have thick, long, double coats. They shed a tremendous amount in the spring. Regular grooming or having their coats clipped in the spring can significantly reduce the tumbleweed blowing through the house.
                        </p>
                        <p>
                            Some will tell you a summer clip will ruin their coat. False. This misinformation stems from confusion between clipping and shaving. Clipping is shortening the coat, the equivalent of a human trim at the hair stylist. Clipping does not damage hair follicles. Our Pyrenees get a 1 1/4" clip every spring. Their coats grow back without issue. Shaving is running a blade along the surface of the skin, removing all fur, leaving bare, smooth skin exposed for that "Mr Clean" look. Shaving absolutely can damage hair follicles, affecting or even preventing regrowth. We had an injury to one of our dogs that required a shave down of the hind quarters and part of the tail. Some of the fur grew back in time, some did not. It's a dice roll for fur regrowth if a shave down is done. Make your veterinarian aware of this should your dog need medical attention for an injury. Clipping is fine but NEVER shave the fur unless it's necessary for the overall health of the dog.
                        </p>
                    </div>
                </Fade>
                <Fade bottom>
                    <div className="about-text">
                        <p>
                            Although we are not an organic farm and will never seek organic status, we strive to manage our farm balancing both natural and conventional methods. We grow and bale our own non GMO orchard grass/alfalfa hay mix, fertilized as needed with chicken manure and refrain from using pesticides/herbicides on our fields. We believe you are what you eat...and that includes livestock.
                            Our animal husbandry protocols do include conventional medications on an as needed basis. We also vaccinate our livestock, considering it cheap insurance.
                        </p>
                        <p>
                            Our deworming protocols include natural methods such as COWP, Sericea Lespedeza, pasture rotation and feeding herbs with anti-parasitic properties, using chemical wormers only when necessary.
                            Feel free to browse our site and learn about our farm and livestock. Email us with any questions or comments. We do our best to respond within 24-48 hours. We value your input and look forward to hearing from you.
                        </p>
                    </div>
                </Fade>
            </div>
            <div className="home-footer">

            </div>
        </div>
    );
}
