import React from 'react';
import Fade from 'react-reveal/Fade';
import Carousel from 'react-multi-carousel';
import TopHeader from '../TopHeader';
import ourgoats from '../../images/ourgoats.JPG';
import ourgoats1 from '../../images/ourgoats1.JPG';
import ourgoats2 from '../../images/ourgoats2.JPG';
import ourgoats3 from '../../images/ourgoats3.JPG';
import ourgoats4 from '../../images/ourgoats4.JPG';
import ourgoats5 from '../../images/ourgoats5.JPG';
import 'react-multi-carousel/lib/styles.css';
import '../../styles/about.less';

export default function TheFarm(props) {
    const responsive = {
        desktop: {
            breakpoint: { max: 5000, min: 3000 },
            items: 3,
            slidesToSlide: 3, // optional, default to 1.
        },
        tablet: {
            breakpoint: { max: 3000, min: 3000 },
            items: 2,
            slidesToSlide: 2, // optional, default to 1.
        },
        mobile: {
            breakpoint: { max: 3000, min: 0 },
            items: 1,
            slidesToSlide: 1, // optional, default to 1.
        },
    };

    return (
        <div>
            <div className="home-content">
                <TopHeader text="The Goats" lineWidth="about-line" />
            </div>
            <div className="block">
                <Carousel
                    swipeable={false}
                    draggable={false}
                    showDots={true}
                    responsive={responsive}
                    ssr={true} // means to render carousel on server-side.
                    infinite={true}
                    // autoPlay={this.props.deviceType !== "mobile" ? true : false}
                    autoPlay={true}
                    autoPlaySpeed={3000}
                    keyBoardControl={true}
                    customTransition="all .5"
                    transitionDuration={3000}
                    containerClass="carousel-container"
                    removeArrowOnDeviceType={["tablet", "mobile"]}
                    deviceType={"desktop"}
                    dotListClass="custom-dot-list-style"
                    itemClass="carousel-item-padding-40-px"
                >
                    <div className="our-goats-photo-container"><img src={ourgoats5} alt="farm" className="our-goats-photo"/></div>
                    <div className="our-goats-photo-container"><img src={ourgoats} alt="farm" className="our-goats-photo"/></div>
                    <div className="our-goats-photo-container"><img src={ourgoats4} alt="farm" className="our-goats-photo"/></div>
                    <div className="our-goats-photo-container"><img src={ourgoats2} alt="farm" className="our-goats-photo"/></div>
                    <div className="our-goats-photo-container"><img src={ourgoats3} alt="farm" className="our-goats-photo"/></div>
                    <div className="our-goats-photo-container"><img src={ourgoats1} alt="farm" className="our-goats-photo"/></div>

                </Carousel>
            </div>
            <div className="home-content">
                <Fade right>
                    <div className="site-content padding-15-top">
                        <p>
                            We breed Standard Nubians and Miniature Nubian dairy goats. Our objective is to breed quality dairy goats with good conformation, breed character and capacious udders that are friendly, easy to handle and have strong parasite resistance. We strive to steadily improve those qualities as we expand our herd.
                            Mini Nubians are a relatively new breed, with a fairly narrow gene pool. Our long term goal with our Mini Nubian program is to expand the gene pool by creating our own line using 100% new genetics from our Standard Nubians and Nigerian Dwarf bucks.
                        </p>
                        <p>
                            Our goats are CAE, CL, Brucellosis & Johne's disease negative. We test all does for CAE annually. Our Standard Nubians are G6S normal. All our goats and kids are well socialized with both people and livestock guardian dogs (click the Great Pyrenees tab for more info about our dogs).
                        </p>
                        <p>
                            We'd like to Thank Tanya Farris of Bulletcreek Nubians for the wonderful additions to our standard Nubian herd. We were lucky enough to be in the right place at the right time when Tanya was consolidating her lines. We were blessed to be able to add so many exceptional goats to our herd.
                        </p>
                    </div>
                </Fade>
            </div>
            <div className="home-footer">

            </div>
        </div>
    );
}
