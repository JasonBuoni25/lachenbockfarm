import React, { Component } from 'react';
import '../../styles/dogs.less';
import TopHeader from '../TopHeader';
import Fade from 'react-reveal/Fade';
import puppieslanding from '../../images/puppieslanding.jpg';

export default class PuppiesPage extends Component {
    state = {
        puppiesForSale: [],
        puppyInfo: ''
    };

    onComponentDidMount() {
        // API calls go here
    }

    render() {
        let { puppiesForSale } = this.state;
        return(
            <>
                <div className="flex-column">
                    <TopHeader text="For Sale" lineWidth="for-sale-line" />
                    <Fade left>
                        <div className="home-header">
                            <img src={puppieslanding} alt="farm" className="table-page-photo"/>
                        </div>
                    </Fade>
                </div>
                <table className="sale-table">
                    <thead>
                        <tr className="sale-table-header">
                            <th></th>
                            <th>Gender</th>
                            <th>Parents</th>
                            <th>Whelp Date</th>
                            <th>Ready Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        { 
                            puppiesForSale.length > 0 && 
                            puppiesForSale.map((puppy) => <PuppiesTableRow puppyRow ={puppy} />) ||
                            <tr className="no-puppy-row"><td colSpan="99">No puppies for sale</td></tr>
                        }
                    </tbody>
                </table>
                <SalesInformation />
            </>
        );
    }
}

function PuppiesTableRow({ puppyRow }) {
    const { parents, gender, whelpDate, readyDate, photos } = puppyRow;
    return (
        <tr></tr>
    );
}

function SalesInformation() {
    return (
        <div className="puppies-sales-info">
            <b>Limited AKC registration: $500<br/>Full AKC registration: $850</b>
            <br/>
            <p>
            Each puppy is wormed  at 3, 6 and 8 weeks old> Neopar vaccine is given at 4 weeks, 5 way Distemper given between 6-7 weeks old. You will also receive completed AKC registration forms, an AKC litter pedigree and enough puppy food to transition the puppy without causing digestive upset. We feed Kirkland Signature Puppy food, a 4.5 star dog food available at Costco. Those buyers getting full registration (breeding rights through AKC) will also receive copies of OFA Hip Certification from the Dam and sire, in some cases we can provide OFA certificates back as far as great granddams and great grandsires.
            Please read our Puppy Sales Policy located in the "Our Sales Policy" tab for more information on providing a home to one of our puppies.
            </p>
            Remember: Dogs are NOT disposable. They're a 12-13 year commitment. Think it through BEFORE you commit. 
            <br/><br/>
            <b>Placing our puppies in an appropriate home is our highest priority. We reserve the right to decline rehoming a puppy into any home we feel is not well suited or a good fit for a giant breed livestock guardian dog.</b>
        </div>
    )
}

