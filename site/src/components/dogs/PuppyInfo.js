import React  from 'react';
import '../../styles/dogs.less';
import TopHeader from '../TopHeader';
import Fade from 'react-reveal/Fade';


export default function PuppyInfo(props) {
    return (
        <div className="flex-column">
            <TopHeader text="Caring for your Puppy" lineWidth="puppy-line" />
            <Fade right>
                <div className="split-container-left-dogs">
                    <div className="site-text-label">Our Philosophy and Care</div>
                    <p>
                        We do not subscribe to the notion that in order for a livestock guardian dog to perform as such, the pup needs to be ignored or denied human contact. Through experience with our own Pyrenees we've found this to be completely false. We believe this is an irresponsible practice and do not recommend it. That sort of sink or swim style is detrimental and sets the dog up for failure from day one.
                    </p>
                    <p>
                        All dogs, even those that live with livestock, require basic hands on care; trimming of nails/dewclaws, removing that foot long twig entangled in that long fluffy tail, treating a wound, giving annual vaccines, moving the dog to other pastures, ect... without human socialization this basic care causes extreme stress to the dog and can lead to injury of the dog, handler or both. If you're dog doesn't know and respect you, be prepared to get bit and bruised during routine handling.
                    </p>
                    <p>
                        Our puppies are socialized to human contact from birth. Dam and puppies spend their first 2-3 weeks together indoors (in our sunroom) to ensure each puppy is nursing well, prevent injury from livestock and protect their undeveloped immune system from exposure to environmental factors that could cause them not to thrive. Puppies are introduced to livestock between 4-6 weeks old, sometimes earlier if we have baby goats around.
                    </p>
                    <p>
                        All our puppies are wormed at 6 weeks old with Fenbendazole or Ivermectin and receive their first vaccines at 7 weeks of age by a certified Veterniary Technician. We do not do puppy wellness checks with a veterinarian. Our puppies will only receive veterinary care if a health issues warrants such. If a buyer wishes to have a puppy seen by a veterinarian prior to purchase, we understand, are more than happy to comply and will take the puppy to the local veterinarian, Villanow Animal Clinic. We are willing to travel to another veterinarian if it's the buyers preference, however there are limits to how far we will travel. The cost of this visit is the buyers responsibility and financial obligation.
                    </p>
                    <p>
                        Puppies are ready for their forever homes at 8 weeks of age. We will not allow any puppy to be rehomed prior to that age as we feel appropriate behaviors and social skills are learned through interactions with littermates and their dam. If you're interested in giving one of our puppies a forever home, please read our sales policy.
                    </p>
                </div>
            </Fade>
            <Fade left>
                <div className="split-container-left-dogs">
                    <div className="site-text-label">Feeding Your Puppy</div>
                    <p>Giant breeds grow at an astonishing rate...almost 100 times their size in less than 1 year (humans can take 18 years to grow 35 fold). Rapid growth means the bones must change quickly, this remarkable rate of growth makes giant breeds sensitive to nutritional imbalances. Diet is directly related to development of bone/hip disorders. Feeding a quality dog food with quality ingredients at appropriate calcium levels is crucial to development of healthy joints and bones. Appropriate calcium levels range between 0.9 and 1.2</p>
                    <p>Avoid supermarket dog foods (and that includes Purina). Most dog foods on supermarket shelves are geared towards humans, not dogs. All those different colored pieces of kibble are designed to appeal to the human eye, to make the dog owner believe it's nutritious (green=vegetables, red=meat, etc...). Instead, their chock full of low quality ingredients that are dyed to fool the consumer. Dogs don't care what color their food is. These foods also contain disguised fillers such as corn, brewers rice and whole wheat flour (empty calories with little nutritional value). They cut corners by using plant based proteins (examples: gluten meal and soy) instead of meat to falsely boost the total protein in order to meet nutritional guidelines for dogs (and that includes Purina). These dogs foods fall short of the mark nutritionally with any dog but when fed to a breed that can out pace Jack's Beanstalk in growth, can cause significant growth issues. </p>
                    <p>We recommend TSC (Tractor Supply Company), PetCo, Petsmart and similar places to purchase your puppies food. All of these companies sell a variety of quality dog foods. They also sell low end dog foods as well, which is why we recommend checking out dogfoodadvisor.com as an educational resource to discern the good brands from the bad.</p>
                    <p>Dogfoodadvisor.com is one of the best dog food resources we've found. The site continually evaluates over 4,500 different dog foods, ingredient by ingredient, explaining what those ingredients really are (you'll be surprised by some), then grades each dog food with a 1 to 5 star rating. The site also contains informative articles and forums. Oh, and while you're on that site, type Purina into the search engine, you'll understand why we mention it by name :)</p>
                </div>
            </Fade>
            <Fade bottom>
                <div className="split-container-right">
                    <div className="site-text-label">Informative Links and Articles</div>
                    <p>Due to their tremendous growth rate, giant breeds have different needs than their smaller counterparts, especially during the first 2 years of life. Because of this we've accumulated some fact and research based articles every Great Pyrenees owner or potential owner should read. We add to this list as we come across information worth sharing.</p>
                    <PuppyLink src="http://www.dogfoodadvisor.com/" text="Best online resource for determining the good dog foods from the bad ones." />
                    <PuppyLink src="http://www.dogfoodadvisor.com/forums/search/feeding+large+and+giant+breed+puppies/" text="Forum discussion on feeding large/giant breeds, contains several additional links." />
                    <PuppyLink src="http://showdogsupersite.com/hips.html" text="Good article explaining how environmental factors affect hip/joint development." />
                    <PuppyLink src="http://journals.plos.org/plosone/article?id=10.1371%2Fjournal.pone.0055937" text="Fabulous research based article explaining why spay/neuter should be postponed till adulthood, which is 2 years old in giant breeds." />
                    <PuppyLink src="http://www.dogfoodadvisor.com/best-dog-foods/best-large-breed-puppy-food/" text="How to Choose the Best Large Breed Puppy Food and Lower Your Dog's Risk of Hip Dysplasia." />
                    <PuppyLink src="http://healthypets.mercola.com/sites/healthypets/archive/2012/04/09/slow-growth-diets-for-giant-breed-puppy.aspx" text="'Why Over Growing Your Large/Giant Breed Puppy is Dangerous' By Dr. Karen Becker. Excellent video contained in this link." />
                    <PuppyLink src="http://www.fda.gov/AnimalVeterinary/SafetyHealth/RecallsWithdrawals/default.htm" text="Up to date FDA website Dog listing dog food/treat recalls." />
                    <PuppyLink src="http://journals.plos.org/plosone/article?id=10.1371%2Fjournal.pone.0055937" text="'Neutering Dogs: Effects on Joint Disorders and Cancers in Golden Retrievers'. Results of study conducted in 2013 showing increased risk of joint disease (incuding hip dysplasia) when spayed/neutered prior to adulthood." />
                    <PuppyLink src="http://onlinelibrary.wiley.com/doi/10.1002/vms3.34/full" text="'Neutering of German Shepherd Dogs: associated joint disorders, cancers and urinary incontinence'. Another study conducted showing significant health risks and joint disorders associated with spay/neuter prior to adulthood." />
                    <PuppyLink src="http://healthypets.mercola.com/sites/healthypets/archive/2013/09/30/neutering-health-risks.aspx" text="'Why I've Had a Change of Heart About Neutering Pets' by Dr. Karen Decker. Video by Dr. Decker explaining the numerous health issues directly related to early spay/neuter." />
                    <PuppyLink src="https://www.parsemusfoundation.org/projects/veterinarian-list/" text="List of veterinarians by state that perform vasectomies and ovary sparing spays, which leaves the endocrine system intact and functioning while preventing the ability to breed. We recommend these procedures instead of traditional spay/neuter." />

                </div>
            </Fade>
        </div>
    );
}


function PuppyLink({ src, text }) {
    const route = () => {
        const win = window.open(src, '_blank');
        win.focus();
    };

    return (
        <>
            <span className="puppy-link" onClick={route}><i className="fas fa-link"></i></span> &nbsp;{text}<br/>
        </>
    );

}
