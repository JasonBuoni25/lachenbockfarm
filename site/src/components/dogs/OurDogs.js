import React from 'react';
import Fade from 'react-reveal/Fade';
import TopHeader from '../TopHeader';
import jethro from '../../images/jethro.jpg';
import reagan from '../../images/reagan.jpg';
import heidi from '../../images/heidi.jpg';
import tonka from '../../images/tonka.jpg';

function DogRow({image, rowElement}) {
    return (
        <tr className="site-row">
            <td><Fade left><img className="site-row-image" src={image} alt='' /></Fade></td>
            <td><Fade right>{rowElement}</Fade></td>
        </tr>
    );
}

export default function OurDogsPage(props) {
    return (
        <div className="flex-column">
            <TopHeader text="Our Dogs" lineWidth="about-line" />
            <table className="site-table">
                <tbody>
                    <DogRow image={jethro} rowElement={textStrings['jethro']}/>
                    <DogRow image={reagan} rowElement={textStrings['reagan']}/>
                    <DogRow image={heidi} rowElement={textStrings['heidi']}/>
                    <DogRow image={tonka} rowElement={textStrings['tonka']}/>
                </tbody>
            </table>
        </div>
    );
}

const textStrings = {
    jethro: <Fade right>
        Jethro is named after a character in the 70's TV show ”The Beverly Hillbillies”. Like the character he was a big, brawny, handsome pup but not very bright. He always managed getting stuck under the hay feeder. Fortunately his brained kicked in as he matured and has come into his own as a guardian dog. 
        < br/>
        Jethro's very attentive and watchful. One of our goats collapsed in the pasture. Jethro ran to her side and laid down next to her, not leaving her side while I ran to the house to call the vet. He earned his gold star in guardianship that day.
        < br/>
        <b>OFA hip rating: GOOD</b>
    </Fade>,
    reagan: <Fade right>
        Reagan is named after former president Ronald Reagan. She's the leader and dominant one among our females. She's a great guardian, the smartest of our girls. She's very cautious, methodical, standoffish at times, but will not hesitate to charge head on towards anything she deems a threat.
        <br/>
        Reagan is very pensive and distant with people she doesn't know but very loving and affection with those she trusts. She tends to have very large litters of 12-13 puppies and nurses and cares for each and every one without issue. She is fiercely protective of her puppies if  a stranger is near.
        <br/>
        <b>OFA hip rating: GOOD</b>
    </Fade>,
    heidi: <Fade right>
        Heidi's named after the song “Heidi Heidi Ho”. For some unknown reason she would just about wag her tail off when she heard me singing it, so it became her name. She showed the most amazing guardian instincts as a young pup.
        <br />
        Heidi's bonded like glue to her goats. Where the goats go, Heidi goes. She literally can't handle being separated from her goat friends. At just 5 months old, Heidi alerted us to a snake just beyond the pasture by running the fence line barking aggressively. She's a wonderful, sweet girl but extremely wary of strangers.
        <br />
        <b>OFA hip rating: GOOD</b>
    </Fade>,
    tonka: <Fade right>
        Tonka was our first Pyrenees and the reason we fell in love with the breed. He showed up one day meandering around the farm. We found his owner and returned him but Tonka ran away and came back to our farm the following day. His previous owner decided Tonka prefered our farm and gave him to us. We're so grateful he did.
        <br />
        Tonka's a massive, muscular dog. He patrols his kingdom often like a foot soldier in search of intruders. Tonka also helps train other dogs by pinning them down when their behavior is inappropriate. Tonka is not part of our breeding program. Because his health history is unknown and he's not registered with the AKC we had him neutered...but we love him to pieces.
    </Fade>
}
