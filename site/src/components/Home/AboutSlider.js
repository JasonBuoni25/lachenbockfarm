import React, { Component } from 'react';
import { Link } from 'react-router-dom'

export default class AboutSlider extends Component {
    constructor(props) {
        super(props);
        this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
    }

    state = {
        height: 0,
        width: 0
    };

    componentDidMount() {
        this.updateWindowDimensions();
        window.addEventListener('resize', this.updateWindowDimensions);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowDimensions);
    }

    updateWindowDimensions() {
        this.setState({ width: window.innerWidth, height: window.innerHeight });
    }

    render() {
        return (
           <>
               <div className="landing-image" style={{ height: this.state.height, width: this.state.width }}></div>
               <div className="landing-container">
                   <span >We raise Standard Nubian goats, Miniature Nubian goats and Great Pyrenees livestock guardian dogs. <br/> We also have several horses and miniature donkeys.</span>
                   <div className="col-md-3 col-sm-3 col-xs-6 landing-link-container">
                       <Link to="/about" className="btn btn-sm animated-button victoria-one">Learn More</Link>
                   </div>
               </div>
           </>
        );
    }
}
