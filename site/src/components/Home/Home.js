import React from 'react';
import Carousel from 'react-multi-carousel';

import AboutSlider from './AboutSlider';
import PuppiesSlider from './PuppiesSlider';
import GoatsForSaleSlider from './GoatsForSaleSlider';

import '../../styles/animate.less';
import 'react-multi-carousel/lib/styles.css';

export default function HomePage(props) {

    const responsive = {
        desktop: {
            breakpoint: { max: 5000, min: 3000 },
            items: 3,
            slidesToSlide: 3, // optional, default to 1.
        },
        tablet: {
            breakpoint: { max: 3000, min: 3000 },
            items: 2,
            slidesToSlide: 2, // optional, default to 1.
        },
        mobile: {
            breakpoint: { max: 3000, min: 0 },
            items: 1,
            slidesToSlide: 1, // optional, default to 1.
        },
    };

    return (
        <Carousel
            swipeable={false}
            draggable={false}
            showDots={true}
            responsive={responsive}
            ssr={true} // means to render carousel on server-side.
            infinite={true}
            // autoPlay={this.props.deviceType !== "mobile" ? true : false}
            autoPlay={true}
            autoPlaySpeed={5000}
            keyBoardControl={true}
            customTransition="all .5"
            transitionDuration={1000}
            containerClass="carousel-container"
            removeArrowOnDeviceType={["tablet", "mobile"]}
            deviceType={"desktop"}
            dotListClass="custom-dot-list-style"
            itemClass="carousel-item-padding-40-px"
        >
            <AboutSlider />
            <PuppiesSlider />
            <GoatsForSaleSlider />
        </Carousel>
    );
}
