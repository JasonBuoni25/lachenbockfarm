import React, { Component } from 'react';
import { Link } from 'react-router-dom'

export default class GoatsForSaleSlider extends Component {
    constructor(props) {
        super(props);
        this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
    }

    state = {
        height: 0,
        width: 0
    };

    componentDidMount() {
        this.updateWindowDimensions();
        window.addEventListener('resize', this.updateWindowDimensions);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowDimensions);
    }

    updateWindowDimensions() {
        this.setState({ width: window.innerWidth, height: window.innerHeight });
    }

    render() {
        return (
            <>
                <div className="landing-image-goats" style={{ height: this.state.height, width: this.state.width }}></div>
                <div className="landing-container">
                    <span >Our objective is to breed quality dairy goats with good conformation, breed character and capacious udders that are friendly, easy to handle and have strong parasite resistance. <br/>We strive to steadily improve those qualities as we expand our herd. </span>
                    <div className="col-md-3 col-sm-3 col-xs-6 landing-link-container">
                        <Link to="/ourgoats" className="btn btn-sm animated-button victoria-one">See our Goats</Link>
                    </div>
                </div>
            </>
        );
    }
}
