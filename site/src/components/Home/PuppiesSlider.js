import React, { Component } from 'react';
import { Link } from 'react-router-dom'

export default class PuppiesSlider extends Component {
    constructor(props) {
        super(props);
        this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
    }

    state = {
        height: 0,
        width: 0
    };

    componentDidMount() {
        this.updateWindowDimensions();
        window.addEventListener('resize', this.updateWindowDimensions);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowDimensions);
    }

    updateWindowDimensions() {
        this.setState({ width: window.innerWidth, height: window.innerHeight });
    }

    render() {
        return (
            <>
                <div className="landing-image-puppies" style={{ height: this.state.height, width: this.state.width }}></div>
                <div className="landing-container">
                    <span >Looking for a new best friend? Try a Great Pyrenees.</span>
                    <div className="col-md-3 col-sm-3 col-xs-6 landing-link-container">
                        <Link to="/puppies" className="btn btn-sm animated-button victoria-one">See our puppies</Link>
                    </div>
                </div>
            </>
        );
    }
}
