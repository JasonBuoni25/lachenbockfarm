import React from 'react';
import Fade from 'react-reveal/Fade';
import TopHeader from './TopHeader';
import '../styles/sales.less';

function DogSalesPolicy() {
    return (
        <div className="split-container-right">
            <div className="site-text-label">Puppies</div>
            If you're interested in one of our available puppies, a $100 NON-REFUNDABLE deposit will reserve the puppy of your choice till pick up time. Deposits can be made in cash, through Paypal or by check. Checks will only be accepted until the puppy is 5 weeks old to allow time for the check to clear our bank prior to puppy pick up time. Balance is due when puppy is picked up via cash or Paypal ONLY. Under no circumstance will we accept a check at pick up time.
            Puppies MUST be picked up between 8-9 weeks of age unless other arrangements have been made in advance. We do not do home delivery, it is the sole responsiblity of the buyer to pick up their puppy at the appropriate time. We make a point to contact every person via email, social media, phone or text (whichever method is prefered) that placed a deposit 7-10 days prior to the 8 week mark to set up a pick up day and time. If you have NOT picked up your puppy by the end of the 9th week without making prior arrangements, your deposit is forfeited and the puppy you reserved immediately becomes available to another potential home.
            After you pick up your puppy, you have 3 business days to take your puppy to the vet for a wellness check. If a congenital issue is found and verified by a licensed, practicing veterinarian, you can return the puppy for a full refund, select another puppy from the litter if one is available, or have first choice in a future litter.
            Most of our dogs have hip certification from the Orthopedic Foundation for Animals (OFA). Those that have not been tested are too young and will be tested at the appropriate time. Dogs need to be a minumum of 24 months old for accurate hip assessment. Since there are other factors (diet, obesity, injury and activity levels) that play a key role in the development of growing hips and joints, we do not guarantee the hip status of any puppy. 
            It's important to us that each puppy leaving our farm will be appropriately cared for and trained. We encourage owners to contact us with any questions they might have along the way. There is a learning curve for the puppy as well as owner. Yes, they're guardian dogs with strong instincts. However training, correction and proper guidance is required. If you don't have the time to invest then another form of livestock protection may better suit your needs.
            <br />
            <span className="sales-puppy">
                * Placing our puppies in an appropriate home is our highest priority. We reserve the right to decline rehoming a puppy into any home we feel is not well suited or a good fit for a giant breed livestock guardian dog.
            </span>
        </div>
    );
}

function GoatSalesPolicy() {
    return (
        <div className="split-container-left">
            <div className="site-text-label">Goat/Kids</div>
            If you're interested in purchasing one of our goats/kids, a $100 NON REFUNDABLE deposit will reserve your goat/kid till pick up time. Balance is due, in cash or through Paypal, when you pick up the goat, we will not accept a check.
            We prefer kids to be picked up within 2 weeks of placing deposit. If that's not possible we are willing to keep kids a bit longer, however, a $3/day per kid fee will be charged to cover the cost of caring for each kid. We will not hold reserved kids beyond 3 months old. If a kid is not picked up by 3 months of age, your deposit and all associated fees are forfeited. Adult goats must be picked up within 2 weeks of placing a deposit or the deposit is forfeited. We also require all goats/kids to be picked up at the farm at an agreed upon time, we will no longer drive to meet people...no exceptions.
            All kids born on our farm are bottle fed from birth. They are disbudded and tattooed within 10 days of kidding and receive coccidia prevention every 3 weeks. All kids are registered with the American Dairy Goat Association, ADGA (standard nubians) or the Miniature Dairy Goat Association (MDGA) for Mini Nubians. Registry with any other organization/entity is the buyers responsibility and financial obligation.
            All does are tested annually for CAE. Johne's, Brucellosis and CL testing is done randomly. Buyers are more than welcome to have any testing done they choose to while the goat/kid is still in our care. The financial obligation for testing is the buyers responsibility. However, should any goat/kid test positive, we will reimburse the buyer for the cost of that particular test and refund the deposit.
            Once a goat/kid leaves our farm, there is no way we can safely determine whether or not it had exposure to CL, Brucellosis or Johne's disease through other goats, livestock or wildlife while in someone elses care.  Quarantine is not a safe option for determining exposure, since flies, foot traffic, rain and even wind can carry disease causing bacteria from a quarantined area to an unquarantined area. CL bacterium and Johne's disease mycobacterium can survive in the ground for years once soil has been contaminated. Under no circumstances will we risk the health status of our entire herd by taking a goat back once it has left our farm even for a brief period of time. We have never had a single case of CAE, CL, Brucellosis or Johne's disease on our farm....we hope to keep it that way, which is why we have a strict no return policy. ALL SALES ARE FINAL, NO EXCEPTIONS. We hope you understand our position.
        </div>
    );
}

export default function SalesPolicy(props) {
    return (
        <div className="sales-content" lineWidth="sales-bar">
            <TopHeader text="Our Sales Policy" />
            <div className="site-text-content">
                <Fade left>
                    <GoatSalesPolicy />
                </Fade>
                <Fade right>
                    <DogSalesPolicy />
                </Fade>
            </div>
        </div>
    )
}