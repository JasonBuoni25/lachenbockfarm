import React, { Component } from 'react';
import '../../styles/dogs.less';
import TopHeader from '../TopHeader';
import Fade from 'react-reveal/Fade';
import banner1 from '../../images/banner1.jpg';
import '../../styles/goats.less';

export default class Kids extends Component {
    state = {
        kidsForSale: [],
        kidInfo: ''
    };

    onComponentDidMount() {
        // API calls go here
    }

    render() {
        let { kidsForSale } = this.state;
        return(
            <>
                <div className="flex-column">
                    <TopHeader text="For Sale" lineWidth="for-sale-line" />
                    <Fade left>
                        <div className="home-header">
                            <img src={banner1} alt="farm" className="table-page-photo"/>
                        </div>
                    </Fade>
                </div>
                <table className="sale-table">
                    <thead>
                    <tr className="sale-table-header">
                        <th></th>
                        <th>Gender</th>
                        <th>Parents</th>
                        <th>Whelp Date</th>
                        <th>Ready Date</th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                        kidsForSale.length > 0 &&
                        kidsForSale.map((kid) => <KidsTableRow kidRow ={kid} />) ||
                        <tr className="no-sale-row"><td colSpan="99">No kids for sale</td></tr>
                    }
                    </tbody>
                </table>
            </>
        );
    }
}

function KidsTableRow({ kidRow }) {
    const { parents, gender, whelpDate, readyDate, photos } = kidRow;
    return (
        <tr></tr>
    );
}
