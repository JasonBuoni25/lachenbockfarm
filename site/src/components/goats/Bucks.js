import React, { Component } from 'react';
import '../../styles/dogs.less';
import TopHeader from '../TopHeader';
import Fade from 'react-reveal/Fade';
import GoatTable from './GoatTable';

//Temp
import rosalita_1 from '../../images/rosalita_1.jpg'

export default class Bucks extends Component {
    state = {
        goats: [
            {
                photo: rosalita_1,
                name: 'Lachenbocks Rosalita Come Out Tonite (Rosie)',
                specs: 'F1, 40/60',
                info: `<div className="goat-info-left">
                            Sire: Gypsy Moon PS Macho Dinero <br/>
                            SS: Gypsy Moon Peso<br/> 
                            SD: Rosasharn's SS Unique 4*D<br/>
                            SSS: CH Rosasharn's SP Shazam *B<br/>
                            SSD:  CH Proctor Hill's BB Charivari<br/>
                            SDS: Rosasharn's TL Summers Sol *S<br/>
                            SDD: Rosasharn's Uni 3*D, 2*M<br/>
                            &nbsp;&nbsp;2002: #1 production & butterfat<br/>
                            &nbsp;&nbsp;2003: record breaking production<br/>
                            &nbsp;&nbsp;2004: #1 production & butterfat<br/>
                            &nbsp;&nbsp;2005: #2 production, #1 butterfat<br/>
                            &nbsp;&nbsp;2006: #4 production, #1 butterfat<br/>
                            &nbsp;&nbsp;2007: #2 production, #2 butterfat<br/>
                        </div>
                        <div className="goat-info-right">
                            SSSS: Rosasharn's SW Sapporo ++B<br/>
                            SSSD: Rosasharn's TL Bewitched 6*D, 6*M<br/>
                            SSDS: Unknown<br/>
                            SSDD: Unknown<br/>
                            SDSS: ARMCH Rosasharn's Tiger L ++B, ++*S<br/>
                            SDSD: ARMCH Rosasharn's  Eclipse 2*D, 2*M<br/>
                            SDDS: ARMCH Goodwood Tom Thumb +*S<br/>
                            SDDD: Rosasharn's Baby USA 2*D
                        </div>`
            },
            {
                photo: rosalita_1,
                name: 'Lachenbocks Rosalita Come Out Tonite (Rosie)',
                specs: 'F1, 40/60',
                info: `<div className="goat-info-left">
                            Sire: Gypsy Moon PS Macho Dinero <br/>
                            SS: Gypsy Moon Peso<br/> 
                            SD: Rosasharn's SS Unique 4*D<br/>
                            SSS: CH Rosasharn's SP Shazam *B<br/>
                            SSD:  CH Proctor Hill's BB Charivari<br/>
                            SDS: Rosasharn's TL Summers Sol *S<br/>
                            SDD: Rosasharn's Uni 3*D, 2*M<br/>
                            &nbsp;&nbsp;2002: #1 production & butterfat<br/>
                            &nbsp;&nbsp;2003: record breaking production<br/>
                            &nbsp;&nbsp;2004: #1 production & butterfat<br/>
                            &nbsp;&nbsp;2005: #2 production, #1 butterfat<br/>
                            &nbsp;&nbsp;2006: #4 production, #1 butterfat<br/>
                            &nbsp;&nbsp;2007: #2 production, #2 butterfat<br/>
                        </div>
                        <div className="goat-info-right">
                            SSSS: Rosasharn's SW Sapporo ++B<br/>
                            SSSD: Rosasharn's TL Bewitched 6*D, 6*M<br/>
                            SSDS: Unknown<br/>
                            SSDD: Unknown<br/>
                            SDSS: ARMCH Rosasharn's Tiger L ++B, ++*S<br/>
                            SDSD: ARMCH Rosasharn's  Eclipse 2*D, 2*M<br/>
                            SDDS: ARMCH Goodwood Tom Thumb +*S<br/>
                            SDDD: Rosasharn's Baby USA 2*D
                        </div>`
            },
            {
                photo: rosalita_1,
                name: 'Lachenbocks Rosalita Come Out Tonite (Rosie)',
                specs: 'F1, 40/60',
                info: `<div className="goat-info-left">
                            Sire: Gypsy Moon PS Macho Dinero <br/>
                            SS: Gypsy Moon Peso<br/> 
                            SD: Rosasharn's SS Unique 4*D<br/>
                            SSS: CH Rosasharn's SP Shazam *B<br/>
                            SSD:  CH Proctor Hill's BB Charivari<br/>
                            SDS: Rosasharn's TL Summers Sol *S<br/>
                            SDD: Rosasharn's Uni 3*D, 2*M<br/>
                            &nbsp;&nbsp;2002: #1 production & butterfat<br/>
                            &nbsp;&nbsp;2003: record breaking production<br/>
                            &nbsp;&nbsp;2004: #1 production & butterfat<br/>
                            &nbsp;&nbsp;2005: #2 production, #1 butterfat<br/>
                            &nbsp;&nbsp;2006: #4 production, #1 butterfat<br/>
                            &nbsp;&nbsp;2007: #2 production, #2 butterfat<br/>
                        </div>
                        <div className="goat-info-right">
                            SSSS: Rosasharn's SW Sapporo ++B<br/>
                            SSSD: Rosasharn's TL Bewitched 6*D, 6*M<br/>
                            SSDS: Unknown<br/>
                            SSDD: Unknown<br/>
                            SDSS: ARMCH Rosasharn's Tiger L ++B, ++*S<br/>
                            SDSD: ARMCH Rosasharn's  Eclipse 2*D, 2*M<br/>
                            SDDS: ARMCH Goodwood Tom Thumb +*S<br/>
                            SDDD: Rosasharn's Baby USA 2*D
                        </div>`
            }
        ],
    };

    onComponentDidMount() {
        // API calls go here
    }

    render() {
        let { goats } = this.state;
        return(
            <>
                <div className="flex-column">
                    <TopHeader text="Bucks" lineWidth="buck-line" />
                </div>
                <GoatTable goats={goats} />
            </>
        );
    }
}
