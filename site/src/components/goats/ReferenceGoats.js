import React, { Component } from 'react';
import '../../styles/dogs.less';
import TopHeader from '../TopHeader';
import Fade from 'react-reveal/Fade';
import GoatTable from './GoatTable';

//Temp
import rosalita_1 from '../../images/rosalita_1.jpg'

export default class ReferenceGoats extends Component {
    state = {
        goats: [
            {
                photo: rosalita_1,
                name: 'Lachenbocks Rosalita Come Out Tonite (Rosie)',
                specs: 'F1, 40/60',
                info: `<div className="goat-info-left">
                            Sire: Gypsy Moon PS Macho Dinero <br/>
                            SS: Gypsy Moon Peso<br/> 
                            SD: Rosasharn's SS Unique 4*D<br/>
                            SSS: CH Rosasharn's SP Shazam *B<br/>
                            SSD:  CH Proctor Hill's BB Charivari<br/>
                            SDS: Rosasharn's TL Summers Sol *S<br/>
                            SDD: Rosasharn's Uni 3*D, 2*M<br/>
                            &nbsp;&nbsp;2002: #1 production & butterfat<br/>
                            &nbsp;&nbsp;2003: record breaking production<br/>
                            &nbsp;&nbsp;2004: #1 production & butterfat<br/>
                            &nbsp;&nbsp;2005: #2 production, #1 butterfat<br/>
                            &nbsp;&nbsp;2006: #4 production, #1 butterfat<br/>
                            &nbsp;&nbsp;2007: #2 production, #2 butterfat<br/>
                        </div>
                        <div className="goat-info-right">
                            SSSS: Rosasharn's SW Sapporo ++B<br/>
                            SSSD: Rosasharn's TL Bewitched 6*D, 6*M<br/>
                            SSDS: Unknown<br/>
                            SSDD: Unknown<br/>
                            SDSS: ARMCH Rosasharn's Tiger L ++B, ++*S<br/>
                            SDSD: ARMCH Rosasharn's  Eclipse 2*D, 2*M<br/>
                            SDDS: ARMCH Goodwood Tom Thumb +*S<br/>
                            SDDD: Rosasharn's Baby USA 2*D
                        </div>`,
                reason: 'Josie was a beautiful doe with an exceptional mammary. For an unknown reason\n' +
                    'she stopped settling when bred. We though she might be cystic, treated for that\n' +
                    'but after 2 unsuccessful years we sold her as a companion goat. She was a\n' +
                    'gallon/day producer. We\'ve retained 2 of her udders for our breeding\n' +
                    'program, 1 full nubian, 1 mini nubian.'
            },
            {
                photo: rosalita_1,
                name: 'Lachenbocks Rosalita Come Out Tonite (Rosie)',
                specs: 'F1, 40/60',
                info: `<div className="goat-info-left">
                            Sire: Gypsy Moon PS Macho Dinero <br/>
                            SS: Gypsy Moon Peso<br/> 
                            SD: Rosasharn's SS Unique 4*D<br/>
                            SSS: CH Rosasharn's SP Shazam *B<br/>
                            SSD:  CH Proctor Hill's BB Charivari<br/>
                            SDS: Rosasharn's TL Summers Sol *S<br/>
                            SDD: Rosasharn's Uni 3*D, 2*M<br/>
                            &nbsp;&nbsp;2002: #1 production & butterfat<br/>
                            &nbsp;&nbsp;2003: record breaking production<br/>
                            &nbsp;&nbsp;2004: #1 production & butterfat<br/>
                            &nbsp;&nbsp;2005: #2 production, #1 butterfat<br/>
                            &nbsp;&nbsp;2006: #4 production, #1 butterfat<br/>
                            &nbsp;&nbsp;2007: #2 production, #2 butterfat<br/>
                        </div>
                        <div className="goat-info-right">
                            SSSS: Rosasharn's SW Sapporo ++B<br/>
                            SSSD: Rosasharn's TL Bewitched 6*D, 6*M<br/>
                            SSDS: Unknown<br/>
                            SSDD: Unknown<br/>
                            SDSS: ARMCH Rosasharn's Tiger L ++B, ++*S<br/>
                            SDSD: ARMCH Rosasharn's  Eclipse 2*D, 2*M<br/>
                            SDDS: ARMCH Goodwood Tom Thumb +*S<br/>
                            SDDD: Rosasharn's Baby USA 2*D
                        </div>`,
                reason: 'Milo was one of our herdsires from 2012-2016. He sired many kids.\n' +
                    'Milo was sold in the fall of 2017 after we retained two daughters,\n' +
                    'He\'s now enjoying life as a herdsire in Tennessee.'
            }
        ],
    };

    onComponentDidMount() {
        // API calls go here
    }

    render() {
        let { goats } = this.state;
        return(
            <>
                <div className="flex-column">
                    <TopHeader text="Reference Goats" lineWidth="ref-line" />
                </div>
                <GoatTable goats={goats} />
                <Fade bottom>
                    <p>
                        Reference goats are no longer part of our herd but have made a positive impact through offspring.
                        Some have played larger roles than others, but all have left their hoof print in some way.
                    </p>
                </Fade>
            </>
        );
    }
}
