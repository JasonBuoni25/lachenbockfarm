import React, { Component } from 'react';
import TopHeader from '../TopHeader';
import Fade from 'react-reveal/Fade';
import '../../styles/goats.less';
import rosalita_1 from "../../images/rosalita_1.jpg";

export default class BreedingSchedule extends Component {
    state = {
        schedule: [
            {
                male: rosalita_1,
                female: rosalita_1,
                kidded: '2/19/2019',
                kids: false
            },
            {
                male: rosalita_1,
                female: rosalita_1,
                kidded: '2/19/2019',
                kids: false
            }
        ]
    };

    onComponentDidMount() {
        // API calls go here
    }

    render() {
        let { schedule } = this.state;
        return(
            <>
                <div className="flex-column">
                    <TopHeader text="Breeding Schedule" lineWidth="breeding-line" />
                    <Fade left>
                        <div className="home-header">
                            {/*<img src={banner1} alt="farm" className="table-page-photo"/>*/}
                        </div>
                    </Fade>
                </div>
                <div className="breeding-content">
                    <table className="breeding-table">
                        <tbody>
                        {
                            schedule.length > 0 &&
                            schedule.map((item) => {
                               return (
                                   <tr>
                                       <td>
                                           <Fade left>
                                               <div className="breeding-img-wrapper">
                                                   <img className="breeding-img" src={item.female} />
                                                   <img className="breeding-img" src={item.male} />
                                               </div>
                                           </Fade>
                                       </td>
                                       <td>
                                           <Fade right>
                                               <div className="breeding-info">
                                                   <div>{item.kidded}</div>
                                                   <div>{item.kids && 'Kids available' || 'No kids available'}</div>
                                               </div>
                                           </Fade>
                                       </td>
                                   </tr>
                               );
                            })
                        }
                        </tbody>
                    </table>
                    <Fade bottom>
                        <p>
                            Since our objective is to continue to expand our herd, we do not reserve kids or accept deposits prior to kidding. We like to take a few weeks, sometimes longer and evaluate kids to decide who will be retained. Never an easy decision and it seems to get more difficult each year.
                        </p>
                        <p>
                            In addition, we find it poor business practice to reserve an unborn kid, allow the buyer to anticipate and plan for that kid only to pull the rug out from under by deciding to retain that particular kid once it's born. That practice has been done to us in the past and while it's routinely done with most livestock breeders we find it to be "unsportsmanlike conduct" and will not conduct business in that manner.
                        </p>
                        <p>
                            If you're interested in adding one of our goats/kids to your herd, let us know. We do keep a list and will contact people, in the order the request was made, after we've made our final decision on what we'll be retaining.
                        </p>
                        <p>
                            We leave nothing to chance...all breeding are confirmed with pregnancy tests.  If a breeding is marked "unconfirmed" we believe that doe is bred but have not yet confirmed the pregnancy through blood testing. Also, unless a particular breeding is marked "confirmed" the selected sire may change without notice.
                        </p>
                    </Fade>
                </div>
            </>
        );
    }
}

function KidsTableRow({ kidRow }) {
    const { parents, gender, whelpDate, readyDate, photos } = kidRow;
    return (
        <tr></tr>
    );
}
