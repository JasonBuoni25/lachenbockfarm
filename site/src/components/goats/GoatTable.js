import React from 'react';
import '../../styles/dogs.less';
import Fade from 'react-reveal/Fade';
import '../../styles/goats.less';
import { getImageFromSting } from '../../utils/images';


export default function GoatTable({ goats }) {
    return (
        <table className="goat-table">
            <th>
                <tr></tr>
                <tr></tr>
            </th>
            <tbody>
            { goats.map((goat, idx) => {
                // const goatImage = getImageFromSting(goat.photo);
                return (
                    <tr>
                        <td className="left-goat-td">
                            <Fade left>
                                <div className="goat-header">{goat.name}</div>
                                <div className="goat-table-spec"><i>{goat.specs}</i></div>
                                <img className="goat-table-image"
                                src={goat.photo}
                                alt='Helpful alt text'/>
                            </Fade>
                        </td>
                        <td>
                            <div className={idx % 2 != 1 && 'goat-content'}>
                                <Fade right>
                                    <div className="flex-row" dangerouslySetInnerHTML={{__html:goat.info}}></div>
                                </Fade>
                            </div>
                        </td>
                    </tr>
                )
            })}
            </tbody>
        </table>
    )
}
