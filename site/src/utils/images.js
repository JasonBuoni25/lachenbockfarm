function arrayBufferToBase64(buffer) {
    let binary = '';
    const bytes = [].slice.call(new Uint8Array(buffer));
    bytes.forEach((b) => binary += String.fromCharCode(b));
    return window.btoa(binary);
}

function getImageFromSting(data) {
    const base64Flag = 'data:image/jpeg;base64,';
    const imageStr = arrayBufferToBase64(data);
    return base64Flag + imageStr;
}

export { getImageFromSting }
