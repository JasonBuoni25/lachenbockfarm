import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/app';
import './styles/styles.less';

ReactDOM.render(<App />, document.getElementById('root'));
